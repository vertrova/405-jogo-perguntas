package com.itau.jogo.Kafka;

import java.util.Properties;
import java.util.Random;
import java.util.concurrent.ExecutionException;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

public class Producer {

	private static final String TOPIC = "game";
	private static final String SERVERS = "10.162.107.229:9090;10.162.107.229:9091;10.162.107.229:9092";

	/**
	 * 
	 * @param index
	 * @return
	 */
	public static KafkaProducer<String, String> createProducer() {
		Properties props = new Properties();
		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, SERVERS);
		props.put(ProducerConfig.CLIENT_ID_CONFIG, "B2");
		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);

		KafkaProducer<String, String> producer = new KafkaProducer<String, String>(props);
		return producer;
	}

	public static void createProducerMensagem(String mensagem) {

		Random random = new Random();
		KafkaProducer<String, String> producer = createProducer();

		final String topico = TOPIC;
		final String key = "B2.log." + random.nextInt(10000000);
		final String body = mensagem;

		ProducerRecord<String, String> record = new ProducerRecord<String, String>(topico, key, body);

		try {
			producer.send(record).get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		producer.flush();
		producer.close();
	}
}
