package com.itau.jogo.controllers;

import java.util.List;
import java.util.Optional;

import javax.jms.JMSException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.itau.jogo.Kafka.Producer;
import com.itau.jogo.models.Jogo;
import com.itau.jogo.models.Questao;
import com.itau.jogo.models.Ranking;
import com.itau.jogo.queue.JogoPost;
import com.itau.jogo.repositories.JogoRepository;

@RestController
public class JogoController {
	
	 @Value("${com.itau.jogo.controllers.queue-ranking}")
	 public String queueSetRanking;

	@Autowired
	JogoRepository jogoRepository;
	
	@Autowired
	JogoPost jogoPost;

	
	@RequestMapping(method = RequestMethod.POST, path = "/api/v1/new")
	public ResponseEntity<?> setNewGame(@RequestBody Jogo jogo) {
		
		Producer.createProducerMensagem("Inicio de jogo");

		String url = "http://localhost:8082/question/random/";
		RestTemplate rest = new RestTemplate();
		ResponseEntity<Questao> response = rest.exchange(url, HttpMethod.GET, null,
				new ParameterizedTypeReference<Questao>() {
				});
		Questao questao = response.getBody();
		jogo.setIdPergunta(questao.getId());
		jogo.setEstado('A');

		// response.getHeaders().add("idJogo",
		// Long.toString(jogoRepository.save(jogo).getId()));

		HttpHeaders headers = new HttpHeaders();
		headers.set("idJogo", Long.toString(jogoRepository.save(jogo).getId()));

		return new ResponseEntity<Jogo>(jogo, headers, HttpStatus.OK);
	}

	@RequestMapping(value = "/api/v1/jogo/questao/", method = RequestMethod.GET)
	public ResponseEntity<?> getJogo(@RequestHeader("idJogo") long idJogo) {

		Optional<Jogo> jogo = null;
		jogo = jogoRepository.findById(idJogo);

		String url = "http://localhost:8082/question/" + jogo.get().getIdPergunta();
		RestTemplate rest = new RestTemplate();
		ResponseEntity<Questao> response = rest.exchange(url, HttpMethod.GET, null,
				new ParameterizedTypeReference<Questao>() {
				});
		Questao questao = response.getBody();

		return ResponseEntity.ok().body(questao);
	}

	@RequestMapping(value = "/api/v1/jogo/enviar/", method = RequestMethod.PUT)
	public ResponseEntity<?> setResposta(@RequestBody String resposta, @RequestHeader("idJogo") long idJogo) throws JMSException {

		Optional<Jogo> jogo = null;
		jogo = jogoRepository.findById(idJogo);

		String url = "http://localhost:8082/question/" + jogo.get().getIdPergunta();
		RestTemplate rest = new RestTemplate();
		ResponseEntity<Questao> response = rest.exchange(url, HttpMethod.GET, null,
				new ParameterizedTypeReference<Questao>() {
				});
		Questao questao = response.getBody();

		int acerto = jogo.get().getAcerto();
		int erro = jogo.get().getErro();

		if (questao.getAnswer() == Integer.parseInt(resposta))
			jogo.get().setAcerto(acerto + 1);
		else
			jogo.get().setErro(erro + 1);

		int qtdeRespondidas = jogo.get().getAcerto() + jogo.get().getErro();

		if (qtdeRespondidas >= jogo.get().getQtdePergunta()) {
			jogo.get().setEstado('F');

			Ranking ranking = new Ranking();
			ranking.setTipoJogo(jogo.get().getTipoJogo());
			ranking.setJogador(jogo.get().getIdJogador());
			ranking.setAcerto(jogo.get().getAcerto());
			ranking.setErro(jogo.get().getErro());
			ranking.setTotal(jogo.get().getAcerto() + jogo.get().getErro());
//			url = "http://localhost:8084/api/v1/ranking";
//			rest.postForEntity(url, ranking, Ranking.class);
			jogoPost.postRanking(ranking, queueSetRanking);
			
		}

		url = "http://localhost:8082/question/random/";
		response = rest.exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<Questao>() {
		});
		questao = response.getBody();

		jogo.get().setIdPergunta(questao.getId());

		jogoRepository.save(jogo.get());

		return ResponseEntity.ok().body(jogo);
	}

	@RequestMapping(value = "/api/v1/jogo/ranking/", method = RequestMethod.GET)
	public ResponseEntity<?> getRanking(@RequestHeader("idRanking") String idJogo) {

		String url = "http://localhost:8084/api/v1/ranking/?search=" + idJogo;
		RestTemplate rest = new RestTemplate();
		ResponseEntity<List<Ranking>> response = rest.exchange(url, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<Ranking>>() {
				});
		List<Ranking> ranking = response.getBody();

		return ResponseEntity.ok().body(ranking);
	}

}
