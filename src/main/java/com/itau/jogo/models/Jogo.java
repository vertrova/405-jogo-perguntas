package com.itau.jogo.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Jogo {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	private long idPergunta;
	private int qtdePergunta;
	private int acerto;
	private int erro;
	private String tipoJogo;
	private String idJogador;
	private char estado;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getIdPergunta() {
		return idPergunta;
	}
	public void setIdPergunta(long idPergunta) {
		this.idPergunta = idPergunta;
	}
	public int getQtdePergunta() {
		return qtdePergunta;
	}
	public void setQtdePergunta(int qtdePergunta) {
		this.qtdePergunta = qtdePergunta;
	}
	public int getAcerto() {
		return acerto;
	}
	public void setAcerto(int acerto) {
		this.acerto = acerto;
	}
	public int getErro() {
		return erro;
	}
	public void setErro(int erro) {
		this.erro = erro;
	}
	public String getTipoJogo() {
		return tipoJogo;
	}
	public void setTipoJogo(String tipoJogo) {
		this.tipoJogo = tipoJogo;
	}
	public String getIdJogador() {
		return idJogador;
	}
	public void setIdJogador(String idJogador) {
		this.idJogador = idJogador;
	}
	public char getEstado() {
		return estado;
	}
	public void setEstado(char estado) {
		this.estado = estado;
	}
}
