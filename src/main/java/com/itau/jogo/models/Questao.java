package com.itau.jogo.models;

public class Questao {
	private long id;
	private String title;
	private String category;
	private String option1;
	private String option2;
	private String option3;
	private String option4;
	private int answer;
	
	public void setId(long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public String getCategory() {
		return category;
	}
	public String getOption1() {
		return option1;
	}
	public String getOption2() {
		return option2;
	}
	public String getOption3() {
		return option3;
	}
	public String getOption4() {
		return option4;
	}
	public int getAnswer() {
		return answer;
	}
	public long getId() {
		return id;
	}
}
