package com.itau.jogo.models;

public class Ranking {

	private long id;
	private String tipoJogo;
	private String jogador;
	private int acerto;
	private int erro;
	private int total;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTipoJogo() {
		return tipoJogo;
	}

	public void setTipoJogo(String tipoJogo) {
		this.tipoJogo = tipoJogo;
	}

	public String getJogador() {
		return jogador;
	}

	public void setJogador(String jogador) {
		this.jogador = jogador;
	}

	public int getAcerto() {
		return acerto;
	}

	public void setAcerto(int acerto) {
		this.acerto = acerto;
	}

	public int getErro() {
		return erro;
	}

	public void setErro(int erro) {
		this.erro = erro;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int pontuacao) {
		this.total = pontuacao;
	}
}
