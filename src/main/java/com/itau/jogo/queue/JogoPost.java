package com.itau.jogo.queue;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.Session;

import org.apache.activemq.filter.function.inListFunction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Component;

import com.itau.jogo.models.Ranking;

@Component
public class JogoPost {
	
    @Autowired
    private JmsTemplate jmsTemplate;

    public void postRanking(Ranking ranking, String queueName) throws JMSException {
        jmsTemplate.setReceiveTimeout(10000);
        
        jmsTemplate.send(queueName, new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
            	
            	Map<String, String> rankingMap = new HashMap<>();
            	rankingMap.put("playerName", ranking.getJogador());
            	rankingMap.put("gameId", ranking.getTipoJogo());
            	rankingMap.put("hits", Integer.toString(ranking.getAcerto()));
            	rankingMap.put("misses", Integer.toString(ranking.getErro()));
            	rankingMap.put("total", Integer.toString(ranking.getTotal()));
                Message message = session.createObjectMessage((Serializable)rankingMap);
                return message;
            }
        });
    }
}
