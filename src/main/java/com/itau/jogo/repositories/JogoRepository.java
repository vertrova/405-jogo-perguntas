package com.itau.jogo.repositories;

import org.springframework.data.repository.CrudRepository;

import com.itau.jogo.models.Jogo;

public interface JogoRepository extends CrudRepository<Jogo, Long> {

}
